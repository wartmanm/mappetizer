import * as sourceMap from "source-map";

import * as process from "process";
import * as fs from "fs";
import * as path from "path";
import { promisify } from "util";

import {
    getPossibleOriginalFiles,
    FileHelper,
} from "./files";
import { initialSymbol } from "./types";
import type { PointKey, NamedPoints } from "./types";
import {
    getGeneratedPoints,
    getOriginalPoints,
    getKeys,
    getEntries,
    sortPoints,
} from "./points";
import { lazy } from "./helpers";
import { annotateJsFiles } from "./reverse";

let mainPromise;
let args = process.argv.slice(2);

let outDir, reverse;
[args, outDir] = getParameter(args, "-o", "--out");
[args, reverse] = getOption(args, "-r", "--rev", "--reverse");
if (reverse) {
    let markAll = false;
    let noNames = false;
    [args, markAll] = getOption(args, "--mark-all");
    [args, noNames] = getOption(args, "--no-names");
    showHelpIfNeeded(args);
    const [generatedFile, sourceMap] = args;
    mainPromise = annotateJsFiles(generatedFile, sourceMap, markAll, noNames, outDir ?? "out");
} else {
    let inline;
    [args, inline] = getOption(args, "--inline");
    showHelpIfNeeded(args);
    const [generatedFile, ...originalFiles] = args;
    mainPromise = run(generatedFile, originalFiles, inline, outDir ?? "out");
}

mainPromise.then(exitCode => {
    process.exit(exitCode);
})
.catch((e) => {
    console.log(e);
    process.exit(1);
});

async function run(generatedFile: string, originalFiles: string[], inline: boolean, outputDir: string) {
    const inputDir = path.dirname(generatedFile);
    const generatedText = await promisify(fs.readFile)(generatedFile, { encoding: "utf-8" });

    const lazyOriginalFilename = lazy(getDefaultOriginalFile, undefined, generatedFile, generatedText);
    const files = new FileHelper(inputDir, outputDir);
    const generatedOutputFilename = path.basename(generatedFile);
    const sourceMapFilename = generatedOutputFilename + ".map";

    const { points: generatedPoints, text: cleanedGeneratedText, filesByKey } = getGeneratedPoints(generatedText, generatedFile, lazyOriginalFilename);

    const allOriginalFiles = Array.from(new Set([
        ...getKeys(filesByKey).map(key => filesByKey[key]),
        ...originalFiles
    ]));
    const pointNamesByFile: { [key in string]: PointKey[] } = {};
    for (let [key, filename] of getEntries(filesByKey)) {
        (pointNamesByFile[filename] ?? (pointNamesByFile[filename] = [])).push(key);
    }

    const originalFilePoints = await Promise.all(allOriginalFiles.map(async filename => {
        let originalText = await files.read(filename, "original file");
        if (!originalText) {
            console.log("Substituting \"@\" instead");
            originalText = "@";
        }
        const pointNames = pointNamesByFile[filename];
        const defaultSymbol = pointNames.length == 1 ? pointNames[0] : initialSymbol;
        return getOriginalPoints(originalText, defaultSymbol, filename, new Set(pointNames), filesByKey);
    }));

    // We enforce that each id may only have one associated filename, so no overlaps are possible.
    const originalPoints: NamedPoints = Object.assign({}, ...originalFilePoints.map(file => file.points));

    let error = false;
    const allKeys: Set<PointKey> = new Set([...getKeys(originalPoints), ...getKeys(generatedPoints)]);
    for (let key of allKeys) {
        const originalLength = originalPoints[key]?.length ?? "<missing>";
        const generatedLength = generatedPoints[key]?.length ?? "<missing>";
        if (generatedLength !== originalLength) {
            console.log(`Mismatched number of points for key \"${key.toString()}\".  Original: ${originalLength}, generated: ${generatedLength}`);
            error = true;
        }
    }

    const generatedPositions = sortPoints(generatedPoints);
    const map = new sourceMap.SourceMapGenerator({
        file: sourceMapFilename,
    });
    for (const pos of generatedPositions) {
        const originalPoint = (originalPoints[pos.name] ?? [])[pos.index];
        if (!originalPoint) {
            continue;
        }
        if (originalPoint.name && pos.point.name && originalPoint.name !== pos.name) {
            console.log(`Mismatched original name for key \"${pos.name.toString()}\" index ${pos.index}.  Original: \"${pos.point.name}\".  Generated: \"${originalPoint.name}\".  Note that it only needs to be specified in one source file!`);
            error = true;
            continue;
        }
        map.addMapping({
            generated: pos.point,
            original: originalPoint,
            name: pos.point.name || originalPoint.name,
            source: filesByKey[pos.name],
        });
    }

    if (error) {
        return 1;
    }


    const originalSaves = originalFilePoints.map(({ text }, index) => {
        return files.write(allOriginalFiles[index], "original file", text);
    });
    await Promise.all(originalSaves);

    if (!inline) {
        await files.write(sourceMapFilename, "sourcemap", map.toString());
        const sourceMapUrl = path.basename(sourceMapFilename);
        await files.write(generatedOutputFilename, "generated file", cleanedGeneratedText + "\n//# sourceMappingURL=" + sourceMapUrl);
    } else {
        const blob = Buffer.from(map.toString(), "utf-8");
        const mapText = "data:application/json;base64," + blob.toString("base64");
        await files.write(generatedOutputFilename, "generated file", cleanedGeneratedText + "\n//# sourceMappingURL=" + mapText);
    }
    return 0;
}

function getOption(args: string[], ...names: string[]): [string[], boolean] {
    const index = args.findIndex(arg => names.includes(arg));
    if (index === -1) {
        return [args, false];
    }
    const newArgs = [...args];
    newArgs.splice(index, 1);
    return [newArgs, true];
}

function getParameter(args: string[], ...names: string[]): [string[], string | undefined] {
    const index = args.findIndex(arg => names.includes(arg));
    if (index === -1) {
        return [args, undefined];
    }
    const newArgs = [...args];
    const [param, option] = newArgs.splice(index, 2);
    return [newArgs, option];
}

function getDefaultOriginalFile(filename: string, text: string): string | null {
    const possibleOriginals = getPossibleOriginalFiles(filename);
    for (let original of possibleOriginals) {
        if (fs.existsSync(original)) {
            return path.basename(original);
        }
    }
    return null;
    let error = `No original file could be found.  Tried ${possibleOriginals.join(", ")}.`;
    if (text.lastIndexOf("//# sourceMappingURL=") !== -1) {
        error += "This file has a source mapping URL already.  Did you mean to pass --reverse?";
    }
    throw new Error(error);
}

function showHelpIfNeeded(args: string[]) {
    if (args.length < 1) {
        console.log("mappetizer [--inline] [--out <dir>] [-r|--reverse] <generated> [...<original>]");
        console.log("or --reverse [--mark-all] [--no-names] [--out <dir>] <generated> [<source-map>]");
        process.exit(1);
    }
}
