Mappetizer is a tool for assembling and disassembling javascript source maps.
Source maps consist of a list of points in a minified, or generated, source file, and the corresponding points in an original file.
Mappetizer takes .js files annotated with these points, and produces a source map containing them.
Alternately, passing --reverse and a js file with a `//# sourceMappingURL` comment will produce the corresponding annotated file.

The annotation format is very simple:

#### @

`@` marks a synchronization point. For example:

##### Original
```js
@var @test@ = "hello world";
@console.log(@test@);
```

##### Generated
```js
@var @x@="hello world";@console.log(@test@);
```

Each @ in the original file is mapped to the matching one in the generated file, in sequence.

#### `@(<id>)@`

Synchronization points can be given IDs, so that they can be placed out of order.  For example:

##### Original
```js
@(line_3)@console.log("line 3");
@(line_2)@console.log("line 2");
@(line_1)@console.log("line 1");
```

##### Generated
```js
@(line_1)@console.log("line 1");
@(line_2)@console.log("line 2");
@(line_3)@console.log("line 3");
```

When stepping through original the source code, it will appear to run backwards.  It's a silly trick, but whatever.

`@` and ID points can be mixed, and the `@`s will match up according to the most recent ID.
IDs can also be reused, which works the same way.

#### `@(<id>,<filename>)

Use this to specify the original file for a point.  This is a real file that will be read for matching points.  Files can only be specified the first time an ID is used, and apply to all instances of that ID, but mor ethan one ID can be used per file.

##### Generated
```js
@(a)@var message = "hello from a.js";
@(b_2)@console.log("hello from b.js");
@(a)@console.log(message);
@(b_1)@console.log("this actually runs last");
```

##### a.js
```js
// When there is only one ID used in a file, it doesn't need to be specified
@var message = "hello from a.js";
@console.log(message);
```

##### b.js
```js
@(b_1)@console.log("this actually runs last");
@(b_2)@console.log("hello from b.js");
```

#### `@(<id>,<filename>,<name>)@`

Source maps have an optional `name` field, which is intended to provide the original name of an identifier.
Both the ID and filename can be left blank to reuse the previous ones and only specify a name: `@(,,<name>)@`.

#### @@

`@` is not widely used in javascript, but if you really need one, escape it as `@@`.
