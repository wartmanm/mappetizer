export type Point = {
    line: number,
    column: number,
    /** The name of the source-mapped variable at this point, to be displayed in the debugger */
    name: string,
    /** The column in the input file, for error messages */
    originalColumn: number,
    file?: string,
};

export const initialSymbol = Symbol("initial");

export type PointKey = string | typeof initialSymbol;
export type NamedPoints = { [key in PointKey]: Point[] };