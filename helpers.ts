// todo: better versions are probably in a million libraries
export function lazy(f: Function, thisValue = undefined, ...args: any[]) {
    let value: any;
    let savedFunction: Function | null = f;
    return function() {
        if (!savedFunction) { return value; }
        value = savedFunction.apply(thisValue, args);
        savedFunction = null;
        return value;
    };
}