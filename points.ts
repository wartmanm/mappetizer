import { initialSymbol } from "./types";
import type { Point, PointKey, NamedPoints } from "./types";
import { lazy } from "./helpers";

export function getKeys<T>(points: { [key in PointKey]?: T }) {
    const keys: PointKey[] = Object.keys(points);
    if (initialSymbol in points) {
        keys.push(initialSymbol);
    }
    return keys;
}

export function getEntries<T>(points: { [key in PointKey]: T }): [PointKey, T][] {
    return getKeys(points).map(key => [key, points[key]]);
}

export function sortPoints(allPoints: NamedPoints) {
    const flatPoints = getKeys(allPoints).flatMap((name) => {
        return allPoints[name].map((point, index) => ({ name: name as PointKey, index, point }));
    });
    flatPoints.sort((a, b) => {
        const { point: { line: lineA, column: columnA } } = a;
        const { point: { line: lineB, column: columnB } } = b;
        return lineA - lineB || columnA - columnB;
    });
    return flatPoints;
}

export function getGeneratedPoints(text: string, filename: string, getOriginalFilename: () => string) {
    const filenames: { [key in PointKey]: string } = Object.create(null);
    const result = getMapPoints(text, initialSymbol, (point, pointName, fileName) => {
        if (!filenames[pointName]) {
            try {
                filenames[pointName] = fileName || getOriginalFilename();
            }
            catch (e) {
                console.log(`${filename}:${point.line}:${point.originalColumn + 1} id ${String(pointName)}: Could not find filename.`);
                throw e;
            }
        } else if (fileName) {
            throw new Error(`Filenames may only be specified on the first use of a an ID. (${String(pointName)} specified ${fileName})`);
        }
        return point;
    });
    return { ...result, filesByKey: filenames }
}

export function getOriginalPoints(text: string, defaultKey: PointKey, filename: string, expectedIds: Set<PointKey>, expectedFilenames: { [Key in PointKey]?: string }) {
    return getMapPoints(text, defaultKey, (point, pointName, fileName) => {
        if (fileName) {
            throw new Error(`Original file ${filename}:${point.line}:${point.originalColumn + 1}: Filenames should not be specified in original files. (${String(pointName)} specified ${fileName})`);
        }
        if (!expectedIds.has(pointName)) {
            const originalFile = expectedFilenames[pointName];
            const expectedLocation = originalFile
                ? `expected in ${originalFile}, not this file`
                : `not found in the original file`;
            throw new Error(`Original file ${filename}:${point.line}:${point.originalColumn + 1} Found ID ${String(pointName)}.  This point ID was ${expectedLocation}.`);
        }
        return point;
    });
}

const getPointRegex = lazy(() => {
    const capture = function (s: string) {
        return `(${s})`;
    }
    const maybe = function (s: string) {
        return `(?:${s})?`;
    }
    const mapName = `.*?`;
    const fileName = `.*?`;
    const pointName = `\\w*`;
    const parameters = capture(pointName) + maybe("," + capture(fileName) + maybe("," + capture(mapName)));
    const point = "@" + maybe(`\\(${parameters}\\)@`);
    const escapedPoint = "(?:\\\\)*\\@";
    return new RegExp(`${point}|${escapedPoint}`, "g");
});

function getMapPoints<T>(text: string, defaultKey: PointKey, callback: (point: Point, pointName: PointKey, fileName: string) => T) {
    let currentPointName: PointKey = defaultKey;
    const allPoints: { [key in PointKey]: T[] } = Object.create(null);

    const cleanedText = text.split(/\r?\n/g).map(function (line, lineIndex) {
        let offset = 0;
        return line.replace(getPointRegex(), function (match, pointName, fileName, mapName, columnIndex) {
            if (match.startsWith("\\")) {
                offset += 1;
                // semantically we want to remove the last backslash, but the first one is equally good
                return match.slice(1);
            }
            if (pointName) {
                currentPointName = pointName;
            }
            if (!allPoints[currentPointName]) {
                allPoints[currentPointName] = [];
            }
            const point = {
                line: lineIndex + 1,
                column: columnIndex - offset,
                name: mapName,
                originalColumn: columnIndex,
            };
            const updatedPoint = callback(point, currentPointName, fileName);
            allPoints[currentPointName].push(updatedPoint);
            offset += match.length;
            return "";
        });
    })
    .join("\n");
    return { points: allPoints, text: cleanedText };
}