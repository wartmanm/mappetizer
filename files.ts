import * as fs from "fs";
import * as path from "path";
import { promisify } from "util";
import * as os from "os";

export class FileHelper {
    private readonly inputDir: string;
    private readonly outputDir: string;
    private readonly outputToSameDirectory: boolean;

    constructor(inputDir: string, outputDir: string) {
        this.inputDir = path.normalize(inputDir);
        this.outputDir = path.normalize(outputDir);
        let outputStat;
        try {
            outputStat = fs.statSync(outputDir);
        } catch {
            /* directory doesn't exist, probably */
        }
        if (outputStat) {
            if (!outputStat.isDirectory()) {
                throw new Error("Output must be a directory");
            }
        }

        this.outputToSameDirectory = outputStat?.ino == fs.statSync(inputDir).ino;
    }

    async read(file: string, description: string) {
        const fullPath = resolveRelative(this.inputDir, file);
        console.log(`Reading ${description} ${fullPath}`);
        return promisify(fs.readFile)(fullPath, { encoding: "utf-8" });
    }

    getFilenameForId(name: string) {
        return this.getSafePath(name).outputRelativePath;
    }

    private getSafePath(name: string) {
        const safeChars = this.removeForbiddenChars(name);
        const safeFilename = this.makeSafeFilename(safeChars);
        const noCollision = this.outputToSameDirectory
            ? safeFilename.replace(/(\.js)?$/, ".out$1")
            : safeFilename;
        const outputPath = resolveRelative(this.outputDir, noCollision);
        const relativePath = path.relative(
            path.toNamespacedPath(this.outputDir),
            path.toNamespacedPath(outputPath));

        const constrainedToOutput = relativePath
            .split(path.sep)
            .filter(part => part != "..")
            .join(path.sep);
        if (constrainedToOutput == relativePath) {
            return { outputRelativePath: noCollision, fullPath: outputPath, safe: true };
        }
        return {
            outputRelativePath: constrainedToOutput,
            fullPath: resolveRelative(this.outputDir, constrainedToOutput),
            safe: false
        };
    }

    private removeForbiddenChars(filename: string) {
        if (os.platform() !== "win32") {
            return filename.replace(/\0/g, "");
        }
        // https://docs.microsoft.com/en-us/windows/win32/fileio/naming-a-file
        return filename.replace(/[\x00-\x1f<>:"\\|?*]/g, "");
    }

    private makeSafeFilename(fullPath: string) {
        if (os.platform() !== "win32") {
            return fullPath;
        }
        const basename = path.basename(fullPath);
        const safeFilename = basename.replace(/^(con|prn|aux|nul|com[1-9]|lpt[1-9])(\.[^.]*)?$/i, "$1_$2");
        return path.join(path.dirname(fullPath), safeFilename);
    }

    async write(name: string, description: string, content: string) {
        const { fullPath: safePath, safe } = this.getSafePath(name);

        if (!safe) {
            console.log(`Not writing ${description} to intended ${name}, instead using ${safePath}`);
        } else {
            console.log(`Writing ${description} ${safePath}`);
        }
        await promisify(fs.mkdir)(path.dirname(safePath), { recursive: true });
        return promisify(fs.writeFile)(safePath, content, { encoding: "utf-8" });
    }
}

export function getPossibleOriginalFiles(filename: string): string[] {
    const substitutes: [RegExp, string][] = [
        [/\bgenerated\b/, "original"],
        [/[.-]min\.js]$/, ".js"],
        [/\.js$/, ".original.js"],
        [/$/, "-original"],
    ];
    return substitutes.map(([match, replacement]) => filename.replace(match, replacement))
        .filter(original => original != filename);
}

/// Combine paths as path.resolve(), but don't include the current working directory.
/// This is much nicer for logging, as it reflects whether you entered an absolute or relative path.
function resolveRelative(...paths: string[]) {
    if (paths.length == 0) {
        return ".";
    }
    let absolute = paths.length - 1;
    for (; absolute >= 0; absolute -= 1) {
        if (path.isAbsolute(paths[absolute])) {
            return path.join(...paths.slice(absolute));
        }
    }
    return path.join(...paths);
}