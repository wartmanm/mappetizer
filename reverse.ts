import { SourceMapConsumer } from "source-map";
import { MappingItem } from "source-map";
import { promisify } from "util";
import * as fs from "fs";
import * as path from "path";

import {
    getPossibleOriginalFiles,
    FileHelper,
} from "./files";
import { PointKey } from "./types";

type AnnotationPoint = {
    line: number,
    column: number,
    annotation: string,
};

export async function annotateJsFiles(generatedFile: string, sourceMapArg: string | undefined, markAll: boolean, noNames: boolean, outputDir: string) {
    const generated = await promisify(fs.readFile)(generatedFile, { encoding: "utf-8" });
    const generatedLines = generated.split(/\r?\n/g);
    let sourceMapUrl: string;
    if (sourceMapArg) {
        sourceMapUrl = path.resolve(sourceMapArg);
    } else {
        let lastLine = generatedLines[generatedLines.length - 1];
        // trailing newline?
        if (lastLine == "") {
            lastLine = generatedLines[generatedLines.length - 2];
        }
        sourceMapUrl = (lastLine.match(/^\/\/# sourceMappingURL=(.*)/) ?? [])[1];
        generatedLines.splice(generatedLines.lastIndexOf(lastLine), 1);
    }
    if (!sourceMapUrl) {
        console.log("No source map found.  Please supply a source map on the command line.");
        process.exit(1);
    }
    const fileHelper = new FileHelper(path.dirname(generatedFile), outputDir);

    const sourceMapData = await readFileOrDataUri(fileHelper, sourceMapUrl, "source map");
    if (!sourceMapData) {
        console.log("Could not read source map");
        return 1;
    }
    const consumer = await new SourceMapConsumer(sourceMapData);

    const shouldMark = markAll
        ? lineMarker()
        : () => false;

    const {
        namedMappingItems,
        keysByFile,
        generatedPoints,
        pointsByFile,
    } = getNamedMappingItems(consumer, shouldMark);
    const files = Object.keys(keysByFile);
    const shouldWriteFilenames = getShouldWriteFilenames(generatedFile, files);
    const shouldWriteIds = files.length > 1 || keysByFile[files[0]].length > 1;

    const seenKeys: Set<string> = new Set();

    const output = writeAnnotations(generatedLines, generatedPoints.map(point => {
        const id = namedMappingItems.get(point);
        const cleanedId = shouldWriteIds ? cleanId(id, keysByFile[point.source], point) : undefined;
        const filename = id && shouldWriteFilenames && !seenKeys.has(id)
            ? fileHelper.getFilenameForId(point.source)
            : undefined;
        const name = noNames ? undefined : point.name;
        return {
            line: point.generatedLine - 1,
            column: point.generatedColumn,
            annotation: writeAnnotation(cleanedId, filename, name),
        };
    }));

    await fileHelper.write(path.basename(generatedFile), "generated file", output);

    let fileOutputPromises = files.map(async filename => {
        const fileText = consumer.sourceContentFor(filename, true) ?? await readFileOrDataUri(fileHelper, filename, "original file");
        const lines = fileText.split(/\r?\n/g);
        const shouldWriteIds = keysByFile[filename].length > 1;
        const output = writeAnnotations(lines, pointsByFile[filename].map(point => {
            const id = namedMappingItems.get(point);
            const cleanedId = shouldWriteIds ? cleanId(id, keysByFile[filename], point) : undefined;
            const name = noNames ? undefined : point.name;
            return {
                line: point.originalLine - 1,
                column: point.originalColumn,
                annotation: writeAnnotation(cleanedId, undefined, name),
            };
        }));
        return fileHelper.write(filename, "original file", output);
    });
    await Promise.all(fileOutputPromises);
    consumer.destroy();
    return 0;
}

function getShouldWriteFilenames(generatedFile: string, originalFiles: string[]) {
    if (originalFiles.length > 1) {
        return true;
    }
    if (originalFiles.length == 0) {
        return false;
    }
    const possibleOriginalFiles = getPossibleOriginalFiles(path.basename(generatedFile));
    const originalFilenameIsGuessable = possibleOriginalFiles.indexOf(originalFiles[0]) != -1;
    return !originalFilenameIsGuessable;
}

function cleanId(key: string | undefined, fileKeys: PointKey[], item: MappingItem) {
    if (key && fileKeys.length == 1) {
        const count_suffix = key.lastIndexOf("_");
        return key.slice(0, count_suffix);
    }
    return key;
}

function writeAnnotations(lines: string[], annotationPoints: AnnotationPoint[]) {
    const reader = spanReader(lines);
    let output = "";
    for (let point of annotationPoints) {
        output += reader(point.line, point.column);
        output += point.annotation;
    }
    output += reader(Infinity, Infinity);
    return output;
}

function lineMarker() {
    let lastLine: number;
    return function (point: MappingItem) {
        const shouldMark = point.originalLine != lastLine;
        lastLine = point.originalLine;
        return shouldMark;
    }
}

function getNamedMappingItems(consumer: SourceMapConsumer, shouldMark: (item: MappingItem) => boolean) {
    const keysByFile: { [key in string]: PointKey[] } = Object.create(null);

    const pointsByFile: { [key in string]: MappingItem[] } = Object.create(null);
    const generatedPoints: MappingItem[] = [];

    consumer.eachMapping(mapping => {
        keysByFile[mapping.source] = [];
        const points = pointsByFile[mapping.source] ?? (pointsByFile[mapping.source] = []);
        points.push(mapping);
        generatedPoints.push(mapping);
    }, undefined, SourceMapConsumer.ORIGINAL_ORDER);

    const indexes = generatedPoints.map((_, index) => index);
    indexes.sort((a, b) => {
        const { generatedLine: lineA, generatedColumn: columnA } = generatedPoints[a];
        const { generatedLine: lineB, generatedColumn: columnB } = generatedPoints[b];
        return lineA - lineB || columnA - columnB;
    });
    let expectedIndex = 0;
    let expectedFile: string | undefined = undefined;
    const namedMappingItems: Map<MappingItem, string> = new Map();
    for (let i = 0; i < generatedPoints.length; i++) {
        const index = indexes[i];
        const point = generatedPoints[i];
        if (index != expectedIndex || expectedFile != point.source || shouldMark(point)) {
            const fileKeys = keysByFile[point.source];
            var trimmedSource = point.source.replace(/\.js$/, "").replace(/\W/g, "_");
            const key = `${trimmedSource}_${fileKeys.length}`;
            fileKeys.push(key);
            namedMappingItems.set(point, key);
        }
        expectedFile = point.source;
        expectedIndex = index + 1;
    }

    // TODO: can we call eachMapping again? Or will that return new objects?
    // For that matter, should we just index namedMappingItems by line/column instead?
    const orderedGeneratedPoints = indexes.map(i => generatedPoints[i]);

    return { namedMappingItems, keysByFile, generatedPoints: orderedGeneratedPoints, pointsByFile };
}

function writeAnnotation(id: string | undefined, filename: string | undefined, name: string | undefined) {
    let output = "@";
    if (id || filename || name) {
        output += "(";
        if (id) {
            output += id;
        }
        if (filename || name) {
            output += ",";
            if (filename) {
                output += filename;
            }
        }
        if (name) {
            output += `,${name}`;
        }
        output += ")@";
    }
    return output;
}

function spanReader(lines: string[]) {
    let line = 0;
    let column = 0;
    return function (newLine: number, newColumn: number) {
        if (newLine >= lines.length) {
            newLine = Math.max(lines.length - 1);
        }
        if (newColumn >= lines[newLine].length) {
            newColumn = Math.max(lines[newLine].length - 1, 0);
        }
        if (newLine < line || (newLine == line && newColumn < column)) {
            throw new Error(`attempting to read backwards: from ${line}:${column} to ${newLine}:${newColumn}`);
        }
        let output = "";
        if (newLine > line) {
            output += lines[line].slice(column) + "\n";
            line += 1;
            output += lines.slice(line, newLine).map(l => l + "\n").join("");
            column = 0;
            line = newLine;
        }
        if (newColumn > column) {
            output += lines[line].slice(column, newColumn);
            column = newColumn;
        }
        return output.replace(/@/g, "\\@");
    }
}

async function readFileOrDataUri(files: FileHelper, uri: string, description: string) {
    if (uri.startsWith("data:")) {
        console.log(`reading ${description} from data URI`);
        const dataIndex = uri.indexOf(",");
        if (uri.slice(0, dataIndex).endsWith(";base64")) {
            return btoa(uri.slice(dataIndex + 1));
        } else {
            return decodeURI(uri.slice(dataIndex + 1));
        }
    } else {
        return files.read(uri, description);
    }

}